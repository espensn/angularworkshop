'use strict';

angular.module('myApp.todomvc')
.filter('visibilityMode', [function() {
  return function(text, visibility) {
      if(visibility) {
          return text.toUpperCase();
      } else {
          return text;
      }
  };
}]);
