'use strict';

angular.module('myApp.todomvc')
.directive('todoPriority', function() {
    return {
        restrict: 'E',
        scope: {
            todos: '=todos'
        },
        template: '<div>Important todos: {{(todos | filter: { priority : true }).length}}</div>'
//        templateUrl: 'todo-priority.html'
    };
});
