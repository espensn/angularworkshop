/*global angular */

/**
 * Services that persists and retrieves TODOs from localStorage
 */
angular.module('myApp.todomvc')
    .factory('todoStorage', function ($http) {
        'use strict';

        return {
            get: function () {
                return $http.get('/todos');
            },

            put: function (todos) {
                if(!todos) return;
                return $http.put('/todos',   {
                        todos: todos
                });
            }
        };

    });
