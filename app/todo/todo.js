angular.module('myApp.todomvc', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/todo', {templateUrl: 'todo/todo.html', controller: 'TodoCtrl'});
        $routeProvider.when('/todo/:status', {templateUrl: 'todo/todo.html', controller: 'TodoCtrl'});
    }])