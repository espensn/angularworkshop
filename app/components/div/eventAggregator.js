'use strict';

angular.module('myApp')
.factory('EventAggregator', function($rootScope) {
    var eventAggregator = {};

    eventAggregator.broadcastEvent = function(event) {
        $rootScope.$broadcast(event);
    };

    eventAggregator.on = function(event, cb) {
        $rootScope.$on(event, cb);
    };

    return eventAggregator;
});