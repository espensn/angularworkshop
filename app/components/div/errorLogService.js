'use strict';

/**
 * Util service for logging error responses from rest-apis to standard logs.
 */
angular.module('myApp').
  service("errorLogService", ['$injector', 'configService', function($injector, configService) {
    var $http;
    this.logAjaxFaults = function(rejection) {
      var payload = {
        requestType: rejection.config.method,
        requestData: JSON.stringify(rejection.config.params),
        requestUrl: rejection.config.url,
        responseStatusCode: rejection.status,
        responseData: JSON.stringify(rejection.data)
      };

      // getting $http from injector becaus of circular dependency with httpErrorHandler
      $http = $http || $injector.get('$http');
      return $http({
        url: configService.getErrorLogUrl(),
        headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=ISO-8859-1'},
        method: 'POST',
        data: payload
      });
    };

    this.logException = function(message, stack, cause) {
      var data = {
        message: message,
        stack: JSON.stringify(stack),
        cause: cause
      };
      // getting $http from injector becaus of circular dependency with httpErrorHandler
      $http = $http || $injector.get('$http');
      return $http({
        url: configService.getExceptionLogUrl(),
        headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=ISO-8859-1'},
        method: 'POST',
        data: data
      });
    };
  }]);

