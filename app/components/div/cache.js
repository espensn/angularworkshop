//Enabling default cache for $http and $resource
angular.module('myApp',[])
  .config(['$httpProvider', function ($httpProvider) {
    // enable http caching
    $httpProvider.defaults.cache = true;
  }])


//Angular's $http has a cache built in. Just set cache to true in its options:
$http.get(url, {cache: true}).success(
  //Hurray!
);

//or

$http({cache: true, url: url, method: 'GET'}).success(
  //Hurray!
);

//Custom
var cache = $cacheFactory('myCache');
var data = cache.get(someKey);
if (!data) {
  $http.get(url).success(function (result) {
    data = result;
    cache.put(someKey, data);
  });
}