/* global angular */
/* jshint maxparams: 5 */

'use strict';

angular.module('myApp').
  directive('percentage', function () {
    var FLOAT_REGEXP = /^\d+((\.|\,)\d{1,3})?$/;
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, controller) {

        var validPercentage = function (number) {
          if (FLOAT_REGEXP.test(number)) {
            var parsedPercentage = parseFloat(number.replace(',', '.'));
            return angular.isNumber(parsedPercentage) && parsedPercentage >= 0 && parsedPercentage <= 100;
          } else {
            return false;
          }
        };

        controller.$parsers.unshift(function (viewValue) {
          if (!viewValue) {
            controller.$setValidity('percent', true);
            return viewValue;
          } else if (validPercentage(viewValue)) {
            controller.$setValidity('percent', true);
            return viewValue.replace(',', '.');
          } else {
            controller.$setValidity('percent', false);
            return undefined;
          }
        });

        controller.$formatters.unshift(function (viewValue) {
          if (!viewValue) {
            controller.$setValidity('percent', true);
            return viewValue;
          } else if (validPercentage(viewValue)) {
            controller.$setValidity('percent', true);
            return viewValue.replace('.', ',');
          } else {
            controller.$setValidity('percent', false);
            return viewValue;
          }
        });
      }
    };
  });