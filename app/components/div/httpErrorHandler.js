/* jshint maxparams: 10 */
'use strict';

/**
 * HttpErrorHandler injected into $httpProvider as a response interceptor.
 * The this errorHandler is responsible for translation all kinds of responses to a common format.
 * This way the client code will be easier to understand and handle.
 *
 * The REST-error structure is based on structure from REST backend.
 *
 * If a 500-series error is received a report button will be added. The button gives the customer a way to report common errors that happens often.
 *
 */
angular.module('myApp').
  factory('httpErrorHandler', ['$injector', '$q', 'errorLogService', 'configService', '$window', function($injector, $q, errorLogService, configService, $window) {
    var $rootScope = $injector.get('$rootScope');

    return {
      'response': function(response) {
        // TODO: add success handler?
        return $q.when(response);
      },
      'responseError': function(rejection) {
        
        // so this little snippet just checks is user is not authorized and reloads the page the user is currently which kicks of a servser side redirect
        // to the login page
        if(rejection.status === 401) {
          $window.location.reload();
        }

        //Logging errors to backend. If the logging call fails we skip.
        if(rejection && rejection.config && rejection.config.url !== configService.getErrorLogUrl()) {
          if(rejection.status !== 0 && rejection.status !== 422) {
            errorLogService.logAjaxFaults(rejection);
          }
        }

        return $q.reject(response);
      }
    };
  }]);

// We have to add the interceptor to the queue as a string because the interceptor depends upon service instances that are not available in the config block.
angular.module('myApp').config(['$httpProvider', function($httpProvider) {
  $httpProvider.interceptors.push('httpErrorHandler');
}]);