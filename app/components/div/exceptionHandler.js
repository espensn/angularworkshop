'use strict';

/**
 * Replace AngularJS $exceptionHandler to log javascript errors to backend.
 */
angular.module('myApp')
  .factory('$exceptionHandler', function ($injector) {
    return function (exception, cause) {
      var errorLogService = $injector.get('errorLogService');
      var $log = $injector.get('$log');
      $log.error(exception);
      errorLogService.logException(exception.message, exception.stack, cause);

    };
  });