/* jshint maxparams: 10 */
'use strict';

/**
 * HttpRequestInterceptor injected into $httpProvider as a request interceptor.
 * The this httpRequestInterceptor is responsible for setting headers needed for calling REST-api's.
 *
 */
angular.module('myApp').
  factory('httpRequestInterceptor', ['$q', '$window', function($q, $window) {
    return {
      'request': function(config) {
        if(config) {
          if(config.headers) {
            if($window.client) {
              config.headers['client'] = $window.client;
            } else {
              config.headers['client'] = 'UNKNOWN';
            }
          }

          // request interceptor. Checks angular calls and if content type is of x-www-form-urlencoded we 
          // serialize the data using jquery.param method.
          if(config.headers && config.headers['Content-Type']) {
            if(config.headers['Content-Type'].indexOf('application/x-www-form-urlencoded')>-1) {
              config.data = encodeURI($.param(config.data));
            }
          }
        }
        return config || $q.when(config);
      }
    };
  }]);

// We have to add the interceptor to the queue as a string because the interceptor depends upon service instances that are not available in the config block.
angular.module('myApp').config(['$httpProvider', function($httpProvider) {
  $httpProvider.interceptors.push('httpRequestInterceptor');
}]);