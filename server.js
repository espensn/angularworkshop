var express = require('express');
var app = express();
var bodyParser = require('body-parser')

var storage = {"todos":[]};

console.log("Registering static hosting on root from: /app")
app.use('/', express.static(__dirname + '/app'));
app.use(bodyParser.json()   );

console.log("Registering endpoint: /todos");
app.get('/todos', function(req, res){
    console.log("Fetching:", storage.todos);
    res.json(storage.todos);
})

app.put('/todos', function(req, res){
    if(req.body.todos) {
        storage.todos = req.body.todos;
        console.log("Storing:", storage.todos);
    }
    res.json(storage.todos);
});

 
app.listen(8000);
